package model.data_structures;

import java.util.Iterator;
import java.util.function.LongToIntFunction;

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo<T> primero;
	private NodoSencillo<T> actual;
	private NodoSencillo<T> ultimo;

	private int longitud;

	public ListaEncadenada(){
		primero = null;
		ultimo = primero;
		actual = primero;
		longitud =0;
	}


	public boolean isEmpty(){
		if(longitud == 0)
			return true;
		else return false;
	}
	

	public T pop(){
	  
		NodoSencillo<T> aux = new NodoSencillo<T>();
		aux = primero;
		primero = primero.darSiguiente();
		longitud--;
		return aux.darItem();
		
	}
	
	public void push(T item){
		
		NodoSencillo<T> aux = new NodoSencillo<T>();
		
		if(primero != null)
		aux.establecerSiguiente(primero);
		
		primero = aux;
		aux.establecerItem(item);
		longitud++;
	}
	
	public void enqueue(T item){
		NodoSencillo<T> oldLast = ultimo;
		ultimo = new NodoSencillo<T>();
		ultimo.establecerItem(item);
		ultimo.establecerSiguiente(null);
		if(isEmpty()) primero  = ultimo;
		else oldLast.establecerSiguiente(ultimo);
		longitud++;
	}
	
	public T dequeue(){
		T item  = primero.darItem();
		primero = primero.darSiguiente();
		if(isEmpty()) ultimo = null;
		longitud--;
		return item;
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>(){
			private NodoSencillo<T> temp = primero;

			public boolean hasNext(){
				return temp != null;
			}

			@Override
			public T next(){
				T resultado = null;
				if(temp != null){
					resultado = temp.darItem();
					temp = temp.darSiguiente();
				}
				return resultado;
			}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) {

		NodoSencillo<T> nuevo = new NodoSencillo<T>();
		nuevo.establecerItem(elem);
		if(primero == null){
			primero = nuevo;
			actual = primero;
			ultimo = primero;
			longitud++;
			return;
		}

		ultimo.establecerSiguiente(nuevo);
		ultimo = nuevo;
		longitud++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub

		if(actual != null){
			actual = primero;
			
			//El contador debe iniciar en 1 para que la pel�cula que se est� buscando coincida con la que se est� retonando
			//Si quedan dudas del funcionamiento pueden inicializarlo en 0, correr las pruebas y hacer debug para que vean el funcionamiento 
			int i =1;
			while(i < pos ){
				actual = actual.darSiguiente();
				i++;
			}
			
			return actual.darItem();
		}
		return null;
		
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return longitud;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actual.darItem();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(actual == ultimo){
			return false;
		}
		actual = actual.darSiguiente();
		return true;

	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(actual == primero){
			return false;
		}

		NodoSencillo<T> temp = new NodoSencillo<T>();
		temp = primero;
		boolean cambiado = false;
		for(int i =0; i < longitud && !cambiado ; i++){

			if(temp.darSiguiente() == actual){
				actual = temp;
				cambiado = true;
			}
			temp = temp.darSiguiente();
		}
		return cambiado;
	}

}
