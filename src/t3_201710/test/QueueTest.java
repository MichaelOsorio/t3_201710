package t3_201710.test;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest extends TestCase {
	private Queue<String> queue;
	
	public void setupEscenario1(){
		queue = new Queue<String>();
	}
	
	public void testQueue(){
		setupEscenario1();
		assertEquals("El tamano de la cola deber�a ser 0",0, queue.size());
		assertEquals("isEmpty deber�a retornal true", true, queue.isEmpty());
		queue.enqueue("test");
		assertEquals("el tama�o de la cola deber�a ser 1",1, queue.size());
		assertEquals("Deber�a devolver 'test'","test",queue.dequeue());
	}
	
}
