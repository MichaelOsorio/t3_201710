package t3_201710.test;


import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase{
	private Stack<String> stack;
	
	public void setupEscenario1(){
		stack = new Stack<String>();
	}
	
	
	
	public void testStack(){
		setupEscenario1();
		
		assertEquals("El tama�o de la pila deber�a ser 0", 0, stack.size());
		assertEquals("est� vac�o deber�a ser true",true,stack.isEmpty());
		stack.push("t");
		assertEquals("El tama�o de la pila deber�a ser 1",1, stack.size());
		assertEquals("Deber�a devolver 't'","t",stack.pop());
		
	}
}
